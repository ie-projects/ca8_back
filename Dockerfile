FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package
RUN mv /home/app/target/*.war /home/app/target/ROOT.war

FROM tomcat
RUN rm -rf /usr/local/tomcat/webapps/ROOT
COPY --from=build /home/app/target/ROOT.war /usr/local/tomcat/webapps/
EXPOSE 8080
CMD ["catalina.sh", "run"]