package org.fn.ie.service;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.fn.ie.domain.*;

import java.sql.SQLException;
import java.util.ArrayList;

@RestController
public class SpringServer {
    @RequestMapping(value = "/restaurant", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Restaurant> getRestaurants()
    {
        return Nikivery.getInstance().getRestaurants();
    }

    @RequestMapping(value = "/food_party", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<PartyRestaurant> getPartyFoodReses()
    {
        return Nikivery.getInstance().getPartyFoodReses();
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserProfile getUserProfile(
            @RequestAttribute("userId") int userId
    )
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().getUserProfile();
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Order> getCart(
            @RequestAttribute(value="userId") int userId
    )
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().getUserProfile().getCart();
    }

    @RequestMapping(value = "/history", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<OrderStatus> getOrderStatuses(
            @RequestAttribute(value="userId") int userId
    )
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().getUserProfile().getDeliveryStates();
    }

    @RequestMapping(value = "/food_party_clock", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public long getFoodPartyClock()
    {
        int partyTime = 15 * 60 - (int) (Nikivery.getInstance().getFoodPartyClock() / 1000);
        return partyTime > 0 ? partyTime: 0;
    }

    @RequestMapping(value = "/receipt/{recNum}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Factor getFactor(
            @RequestAttribute(value="userId") int userId,
            @PathVariable(value = "recNum") int recNum)
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().getUserProfile().getFactor(recNum);
    }

    @RequestMapping(value = "/restaurant/{resid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Restaurant getRestaurants(@PathVariable(value = "resid") String resid)
    {
        return Nikivery.getInstance().getRestaurant(resid);
    }

    @RequestMapping(value = "/order", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public int addToCart(
            @RequestAttribute(value="userId") int userId,
            @RequestParam(value = "foodName") String foodName,
            @RequestParam(value = "restaurantId") String restaurantId,
            @RequestParam(value = "isParty") boolean isParty)
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().addToCart(foodName, restaurantId, isParty);
    }

    @RequestMapping(value = "/plus", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public int incrementInCart(
            @RequestAttribute(value="userId") int userId,
            @RequestParam(value = "foodName") String foodName)
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().incrementInCart(foodName);
    }

    @RequestMapping(value = "/minus", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int decrementInCart(
            @RequestAttribute(value="userId") int userId,
            @RequestParam(value = "foodName") String foodName)
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().decrementInCart(foodName);
    }

    @RequestMapping(value = "/recharge", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public int addCredit(
            @RequestAttribute(value="userId") int userId,
            @RequestParam(value = "upCredit") int upCredit)
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().getUserProfile().addCredit(upCredit);
    }

    @RequestMapping(value = "/search", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Restaurant> search(
            @RequestParam(value = "food") String foodName,
            @RequestParam(value = "restaurant") String resName,
            @RequestParam(value = "page") int page)
    {
        ArrayList<Restaurant> test;
        try {
            test = Nikivery.getInstance().search(foodName, resName, page * 8, 8);
            System.out.println("SearchRes: " + test.size());
            return test;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<Restaurant>();
    }

    @RequestMapping(value = "/page/{pageNum}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Restaurant> getRestaurantsPage(
            @PathVariable(value = "pageNum") int pageNum)
    {
        try {
            return DatabaseInterface.getInstance().fetchRestaurants(pageNum * 8, 8);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<Restaurant>();
    }

    @RequestMapping(value = "/finalize", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public int finalizeOrder(
            @RequestAttribute(value="userId") int userId
            )
    {
        Nikivery.getInstance().getUserProfile(userId);
        return Nikivery.getInstance().finalizeOrder();
    }

    @RequestMapping(value = "/signup", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public int signUp(
            @RequestParam(value = "firstName") String firstName,
            @RequestParam(value = "lastName") String lastName,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "email") String email)
    {
        return DatabaseInterface.getInstance().addUser(firstName, lastName, password, phone, email);
    }

    @RequestMapping(value = "/login", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public String login(
            @RequestParam(value = "password") String password,
            @RequestParam(value = "email") String email)
    {
        return DatabaseInterface.getInstance().login(email, password);
    }

    @RequestMapping(value = "/inside_check", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public int insideCheck(
            @RequestAttribute(value="userId") int userId
    )
    {
        if(userId < 0)
            return 1;
        return 2;
    }

    @RequestMapping(value = "/google", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public String googleLogin(
            @RequestParam(value = "token") String googleToken)
    {
        return DatabaseInterface.getInstance().googleLogin(googleToken);
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public int test()
    {
        System.out.println("Server has been contacted through test!");
        System.out.println("Database connection is " + (DatabaseInterface.getInstance().test()==0 ? "on": "off"));
        return 1;
    }
}
