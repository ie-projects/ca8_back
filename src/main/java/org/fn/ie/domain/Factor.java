package org.fn.ie.domain;

import java.util.ArrayList;

public class Factor {
    private String restaurantName;
    private ArrayList<Order> orders;
    private int netPrice;

    public Factor(String restaurantName, ArrayList<Order> orders, int netPrice) {
        this.restaurantName = restaurantName;
        this.orders = orders;
        this.netPrice = netPrice;
    }

    public Factor() {
        this.restaurantName = "";
        this.orders = new ArrayList<Order>();
        this.netPrice = 0;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public int getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(int netPrice) {
        this.netPrice = netPrice;
    }
}
