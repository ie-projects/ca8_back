package org.fn.ie.domain;

public class Order {
    private String foodName;
    private int count;
    private int price;

    public Order(String foodName, int count, int price)
    {
        this.foodName = foodName;
        this.count = count;
        this.price = price;
    }

    public String getFoodName() { return foodName; }

    public int getCount() { return count; }

    public int getPrice() { return price; }
}
