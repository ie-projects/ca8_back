package org.fn.ie.domain;

import java.util.ArrayList;

public class Restaurant {
    private String id;
    private String name;
    private Location location;
    private String logo;
    private ArrayList<Food> menu;
    private ArrayList<PartyFood> partyMenu;

    public void copyRestaurant(PartyRestaurant partyRes)
    {
        this.id = partyRes.getId();
        this.partyMenu = partyRes.getMenu();
        this.location = partyRes.getLocation();
        this.logo = partyRes.getLogo();
        this.menu = new ArrayList<Food>();
        this.name = partyRes.getName();
    }

    public void addFood(Food newFood)
    {
        menu.add(newFood);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ArrayList<Food> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<Food> menu) {
        this.menu = menu;
    }

    public ArrayList<PartyFood> getPartyMenu() {
        return partyMenu;
    }

    public void setPartyMenu(ArrayList<PartyFood> partyMenu) {
        this.partyMenu = partyMenu;
    }
}
